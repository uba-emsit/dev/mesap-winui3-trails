﻿using CommunityToolkit.Mvvm.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using Microsoft.UI.Xaml.Shapes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using UBA.MesapModel;
using UBA.MesapModel.SQLServer;
using UBA.MesapModel.File;
using Microsoft.Windows.ApplicationModel.Resources;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace MesapExplorerNG
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when the application is launched.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(Microsoft.UI.Xaml.LaunchActivatedEventArgs args)
        {           
            Ioc.Default.ConfigureServices(new ServiceCollection()
                .AddSingleton<IDatabaseConnection, SQLServerDatabaseConnection>()
                //.AddSingleton<IDatabaseConnection, FileDatabaseConnection>()
                .AddTransient<MainViewModel>()
                .BuildServiceProvider());
            Ioc.Default.GetService<IDatabaseConnection>()
                .AnnouceClientId(AppResourceManager.Instance.GetString("Application.Name"));

            m_window = new MainWindow();
            m_window.Activate();
        }

        private MainWindow m_window;
    }

    public sealed class AppResourceManager
    {
        private static AppResourceManager instance = null;
        private static ResourceManager resourceManager = null;
        private static ResourceContext resourceContext = null;

        public static AppResourceManager Instance
        { 
            get
            { 
                instance ??= new AppResourceManager();
                return instance;
            } 
        }

        private AppResourceManager()
        {
            resourceManager = new ResourceManager();
            resourceContext = resourceManager.CreateResourceContext();
        }

        public string GetString(string key)
        {
            return resourceManager.MainResourceMap
                .GetValue($"Resources/{key.Replace(".", "/")}", resourceContext).ValueAsString;
        }
    }
}
