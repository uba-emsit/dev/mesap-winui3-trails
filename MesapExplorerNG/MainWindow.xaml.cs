using CommunityToolkit.Mvvm.DependencyInjection;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using UBA.MesapModel;
using CommunityToolkit.Mvvm.Messaging;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace MesapExplorerNG
{
    /// <summary>
    /// An empty window that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainWindow : Window
    {
        private MainViewModel Model { get; set; }

        public MainWindow()
        {
            this.InitializeComponent();
            this.Closed += OnClosed;

            WeakReferenceMessenger.Default.Register<LoadDatabaseListStart>(this,
                (sender, message) =>
                {
                    LoadDatabaseListProgress.Visibility = Visibility.Visible;
                });
            WeakReferenceMessenger.Default.Register<LoadDatabaseListEnd>(this,
                (sender, message) =>
                {
                    LoadDatabaseListProgress.Visibility = Visibility.Collapsed;
                });
            WeakReferenceMessenger.Default.Register<DatabaseSelected>(this,
                (sender, message) =>
                {
                    MainTabView.SelectedIndex = 1;
                });

            Model = Ioc.Default.GetService<MainViewModel>();
            Model.LoadDatabaseListCommand.Execute(null);
        }

        private void NavItemSelected(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            switch (args.InvokedItemContainer.Tag)
            {
                case "Database":
                    MainTabView.SelectedIndex = 0;
                    break;
                case "Overview":
                    MainTabView.SelectedIndex = 1;
                    break;
                case "Search":
                    MainTabView.SelectedIndex = 2;
                    break;
                case "Cube":
                    MainTabView.SelectedIndex = 3;
                    break;
                case "TimeSeries":
                    MainTabView.SelectedIndex = 4;
                    break;
                case "Reports":
                    MainTabView.SelectedIndex = 5;
                    break;
                case "Events":
                    MainTabView.SelectedIndex = 6;
                    break;
                case "Calc":
                    MainTabView.SelectedIndex = 7;
                    break;
                case "MasterData":
                    MainTabView.SelectedIndex = 8;
                    break;
                case "Docu":
                    MainTabView.SelectedIndex = 9;
                    break;
                case "Admin":
                    MainTabView.SelectedIndex = 10;
                    break;
            }
        }

        public void OnClosed(object sender, WindowEventArgs e)
        {
            Model.Disconnect();
        }

        private void DataGrid_AutoGeneratingColumn(object sender, CommunityToolkit.WinUI.UI.Controls.DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Values")
                e.Cancel = true;
            else if (e.PropertyName == "Name")
                e.Column.DisplayIndex = 0;
            else if (e.PropertyName == "Id")
                e.Column.DisplayIndex = 1;
            else if (e.PropertyName == "Unit")
            {
                e.Column.DisplayIndex = 2;
                //e.Column.CellStyle.SetValue(HorizontalAlignment.Center);
            }
            else if (e.PropertyName == "Calculated")
                e.Column.DisplayIndex = 3;
        }
    }
}
