﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.UI.Dispatching;
using Microsoft.UI.Xaml.Controls;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UBA.MesapModel;

namespace MesapExplorerNG
{
    internal partial class MainViewModel : ObservableObject
    {
        private IDatabaseConnection connection;
        private DispatcherQueue dispatcherQueue = DispatcherQueue.GetForCurrentThread();

        public ObservableCollection<BindableProjectDatabase> Databases { get; } = new ObservableCollection<BindableProjectDatabase>();
        
        [ObservableProperty]
        private BindableProjectDatabase database;
        [ObservableProperty]
        private bool connected;

        public AsyncRelayCommand LoadDatabaseListCommand { get; set; }
        public RelayCommand<SelectionChangedEventArgs> OpenDatabaseCommand { get; set; }

        public DataCubeViewModel Cube = new DataCubeViewModel();

        public MainViewModel(IDatabaseConnection connection)
        {
            this.connection = connection;

            LoadDatabaseListCommand = new AsyncRelayCommand(LoadDatabaseList);
            OpenDatabaseCommand = new RelayCommand<SelectionChangedEventArgs>(OpenDatabase);
        }

        private async Task LoadDatabaseList()
        {
            WeakReferenceMessenger.Default.Send(new LoadDatabaseListStart());

            Databases.Clear();
            await Task.Run(() =>
            {
                foreach (BindableProjectDatabase database in connection.LoadProjectDatabaseList())
                {
                    dispatcherQueue.TryEnqueue(() => Databases.Add(database));
                }
            });

            WeakReferenceMessenger.Default.Send(new LoadDatabaseListEnd());
        }

        private void OpenDatabase(SelectionChangedEventArgs selection)
        {
            Database = selection.AddedItems.Count > 0 ? (BindableProjectDatabase) selection.AddedItems[0] : null;
            Connected = Database != null;

            Cube.OnDatabaseChanged(Database);
            
            WeakReferenceMessenger.Default.Send(new DatabaseSelected());
        }

        internal void Disconnect()
        {
            connection.CloseConnection();
        }
    }
    public class LoadDatabaseListStart
    {
        public LoadDatabaseListStart() { }
    }

    public class LoadDatabaseListEnd
    {
        public LoadDatabaseListEnd() { }
    }

    public class DatabaseSelected
    {
        public DatabaseSelected() { }
    }
}