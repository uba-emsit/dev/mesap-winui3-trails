﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.WinUI;
using Mesap.Framework;
using Microsoft.UI.Dispatching;
using Microsoft.UI.Xaml.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBA.MesapModel;

namespace MesapExplorerNG
{
    internal partial class DataCubeViewModel : ObservableObject
    {

        private DispatcherQueue dispatcherQueue = DispatcherQueue.GetForCurrentThread();

        public ObservableCollection<BindableDimension> Dimensions { get; } = new ObservableCollection<BindableDimension>();
        public ObservableCollection<BindableDescriptor> Descriptors { get; } = new ObservableCollection<BindableDescriptor>();
        public ObservableCollection<BindableTree> Trees { get; } = new ObservableCollection<BindableTree>();
        public ObservableCollection<BindableTimeseries> Timeseries { get; } = new ObservableCollection<BindableTimeseries>();

        [ObservableProperty]
        private BindableDimension selectedDimension;

        public RelayCommand<SelectionChangedEventArgs> SelectDimensionCommand { get; set; }

        private BindableProjectDatabase database;

        public DataCubeViewModel()
        {
            SelectDimensionCommand = new RelayCommand<SelectionChangedEventArgs>(SelectDimension);
        }
        
        internal void OnDatabaseChanged(BindableProjectDatabase database)
        {
            this.database = database;

            Dimensions.Clear();
            Descriptors.Clear();
            Trees.Clear();
            
            if (database != null)
                Task.Run(() =>
                {
                    foreach (BindableDimension dimension in database.LoadDimensions())
                    {
                        dispatcherQueue.TryEnqueue(() => Dimensions.Add(dimension));
                    }
                    foreach (BindableTimeseries series in database.LoadTimeseries("DEFAULT_VIEW"))
                    {
                        dispatcherQueue.TryEnqueue(() => Timeseries.Add(series));
                    }
                });
        }

        private void SelectDimension(SelectionChangedEventArgs selection)
        {
            SelectedDimension = selection.AddedItems.Count > 0 ? (BindableDimension)selection.AddedItems[0] : null;
            Descriptors.Clear();
            Trees.Clear();

            if (SelectDimension != null)
                Task.Run(() =>
                {
                    foreach (BindableDescriptor descriptor in database.LoadDescriptors(SelectedDimension))
                    {
                        dispatcherQueue.TryEnqueue(() => Descriptors.Add(descriptor));
                    }
                    foreach (UBA.MesapModel.BindableTree tree in database.LoadTrees(SelectedDimension))
                    {
                        dispatcherQueue.TryEnqueue(() => Trees.Add(tree));
                    }
                });
        }
    }
}
