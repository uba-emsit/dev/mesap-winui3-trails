﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.UI.Dispatching;
using Microsoft.UI.Xaml.Controls;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using UBA.MesapModel;

namespace OpenDatabaseDialog
{
    public partial class OpenDatabaseViewModel : ObservableObject
    {
        private IDatabaseConnection connection;

        [ObservableProperty]
        private BindableProjectDatabase selectedDatabase;
        [ObservableProperty]
        private BindableProjectDatabase openedDatabase;

        public ObservableCollection<BindableProjectDatabase> Databases { get; } = new ObservableCollection<BindableProjectDatabase>();
        
        public AsyncRelayCommand LoadDatabaseListCommand { get; set; }
        public RelayCommand<SelectionChangedEventArgs> SelectDatabaseCommand { get; set; }
        public AsyncRelayCommand OpenDatabaseCommand { get; set; }

        private DispatcherQueue dispatcherQueue = DispatcherQueue.GetForCurrentThread();

        public OpenDatabaseViewModel(IDatabaseConnection connection)
        {
            this.connection = connection;

            LoadDatabaseListCommand = new AsyncRelayCommand(LoadDatabaseList);
            SelectDatabaseCommand = new RelayCommand<SelectionChangedEventArgs>(SelectDatabase);
            OpenDatabaseCommand = new AsyncRelayCommand(OpenSelectedDatabase, () => SelectedDatabase != null);
        }

        private async Task LoadDatabaseList()
        {
            WeakReferenceMessenger.Default.Send(new LoadDatabaseListStart());
            
            await Task.Run(() =>
            {
                dispatcherQueue.TryEnqueue(() => Databases.Clear());
                foreach (BindableProjectDatabase database in connection.LoadProjectDatabaseList())
                {
                    dispatcherQueue.TryEnqueue(() => Databases.Add(database));                    
                }
            });            

            WeakReferenceMessenger.Default.Send(new LoadDatabaseListEnd());
        }

        private void SelectDatabase(SelectionChangedEventArgs selection)
        {
            if (selection.AddedItems.Count == 0)
                SelectedDatabase = null;
            else 
                SelectedDatabase = (BindableProjectDatabase) selection.AddedItems.ElementAt(0);

            OpenDatabaseCommand.NotifyCanExecuteChanged();
        }

        private async Task OpenSelectedDatabase()
        {
            Debug.WriteLine($"Opening selected database {SelectedDatabase.Name}");
            OpenedDatabase = SelectedDatabase;
        }

        internal void Disconnect()
        {
            connection.CloseConnection();
        }
    }

    public class LoadDatabaseListStart
    {
        public LoadDatabaseListStart() { }
    }

    public class LoadDatabaseListEnd
    {
        public LoadDatabaseListEnd() { }
    }
}
