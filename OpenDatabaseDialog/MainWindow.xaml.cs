using CommunityToolkit.Mvvm.DependencyInjection;
using CommunityToolkit.Mvvm.Messaging;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace OpenDatabaseDialog
{
    /// <summary>
    /// An empty window that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainWindow : Window
    {
        public OpenDatabaseViewModel ViewModel { get; set; }

        public MainWindow()
        {
            this.InitializeComponent();
            this.Closed += OnClosed;

            ViewModel = Ioc.Default.GetService<OpenDatabaseViewModel>();

            WeakReferenceMessenger.Default.Register<LoadDatabaseListStart>(this,
                (sender, message) =>
                {
                    LoadListButton.IsEnabled = false;
                    LoadListProgress.Visibility = Visibility.Visible;
                });
            WeakReferenceMessenger.Default.Register<LoadDatabaseListEnd>(this,
                (sender, message) =>
                {
                    LoadListButton.Content = "Reload"; 
                    LoadListButton.IsEnabled = true;
                    LoadListProgress.Visibility = Visibility.Collapsed;
                });
        }

        public void OnClosed(object sender, WindowEventArgs e)
        {
            ViewModel.Disconnect();
        }
    }
}
