﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBA.MesapModel
{
    public interface IDatabaseConnection
    {
        public void AnnouceClientId(String name);

        //public SystemDatabase LoadSystemDatabase();
        
        public List<BindableProjectDatabase> LoadProjectDatabaseList();

        public void CloseConnection();
    }
}
