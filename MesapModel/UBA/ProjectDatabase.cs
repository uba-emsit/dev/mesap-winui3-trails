﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;

namespace UBA.MesapModel
{
    public abstract partial class BindableProjectDatabase : BindableEntity
    {
        [ObservableProperty]
        private string? guid;

        [ObservableProperty]
        private string? description;

        public string IdentityInfo
        {
            get
            {
                return $"{Guid}"; 
            }
        }

        public abstract List<BindableDimension> LoadDimensions();

        public abstract List<BindableDescriptor> LoadDescriptors(BindableDimension dimension);

        public abstract List<BindableTree> LoadTrees(BindableDimension dimension);

        public abstract List<BindableTimeseries> LoadTimeseries(string viewId);
    }
}
