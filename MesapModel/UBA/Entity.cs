﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBA.MesapModel
{
    public abstract partial class BindableEntity : ObservableObject
    {
        [ObservableProperty]
        private string? name;

        [ObservableProperty]
        private string? id;
    }

    public abstract partial class BindableDimension : BindableEntity { }

    public abstract partial class BindableDescriptor : BindableEntity { }

    public abstract partial class BindableTree : BindableEntity { }

    public abstract partial class BindableTimeseries : BindableEntity
    {
        [ObservableProperty]
        private string? unit;

        [ObservableProperty]
        private bool calculated = false;

        [ObservableProperty]
        private List<float> values = new();
    }
}
