﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using SQLServerMesapModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Unity.Resolution;

namespace UBA.MesapModel.SQLServer
{
    internal class ProjectDatabaseImpl : BindableProjectDatabase
    {
        private Application app;
        private UserContext userContext;
        private DatabaseUserContext? databaseUserContext;
        private Database? database;

        internal ProjectDatabaseImpl(Application app, UserContext userContext, DatabaseInfo info)
        {
            this.app = app;
            this.userContext = userContext;

            Name = info.Name;
            Id = info.Id;
            Guid = info.DatabaseGuid;
            Description = info.Description;
        }

       public override List<BindableDimension> LoadDimensions()
        {
            AssureDatabaseOpen();

            List<BindableDimension> dimensions = new List<BindableDimension>();
            foreach (Dimension dimension in database!.Dimensions.OrderBy(item => item.SortNr))
                dimensions.Add(new DimensionViewImpl(dimension));

            return dimensions;
        }

        public override List<BindableDescriptor> LoadDescriptors(BindableDimension dimension)
        {
            AssureDatabaseOpen();

            List<BindableDescriptor> descriptors = new List<BindableDescriptor>();
            foreach (Descriptor descriptor in database!.GetDescriptors(database!.Dimensions[dimension.Id]).OrderBy(item => item.SortNr))
                descriptors.Add(new DescriptorViewImpl(descriptor));

            return descriptors;
        }

        public override List<BindableTree> LoadTrees(BindableDimension dimension)
        {
            AssureDatabaseOpen();

            List<BindableTree> trees = new List<BindableTree>();
            foreach (Tree tree in database!.Trees.Where(tree => tree.Dimension.Id == dimension.Id).OrderBy(item => item.SortNr))
                trees.Add(new TreeViewImpl(tree, databaseUserContext));

            return trees;
        }

        public override List<BindableTimeseries> LoadTimeseries(string viewId)
        {
            AssureDatabaseOpen();
            List<BindableTimeseries> result = new List<BindableTimeseries>();

            TimeseriesView view = new TimeseriesViewReader(databaseUserContext).Read(new[] { viewId }).First();
            foreach (Timeseries series in new TimeseriesReader(databaseUserContext).Read(view.TimeseriesFilterSettings))
            {
                result.Add(new TimeseriesViewImpl(series));
            }

            return result;
        }

        private void AssureDatabaseOpen()
        {
            if (databaseUserContext == null)
            {
                databaseUserContext = app.OpenDatabase(Id, userContext);
                database = new Database(databaseUserContext);
            }
        }        
    }

    internal class DimensionViewImpl : BindableDimension
    {
        public DimensionViewImpl(Dimension dimension)
        {
            Name = dimension.Name;
            Id = dimension.Id;
        }
    }

    internal class DescriptorViewImpl : BindableDescriptor
    {
        public DescriptorViewImpl(Descriptor descriptor)
        {
            Name = descriptor.Name;
            Id = descriptor.Id;
        }
    }

    internal class TreeViewImpl : BindableTree
    {
        private Tree _tree;
        private DatabaseUserContext _context;

        public TreeViewImpl(Tree tree, DatabaseUserContext context)
        {
            _tree = tree;
            _context = context;
            
            Name = tree.Name;
            Id = tree.Id;
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            System.Threading.Tasks.Task.Run(() =>
            {
                EditableTree etree = new(_tree);
                etree.Tree.Name = Name;
                TreeWriter writer = new(_context);

                Debug.WriteLine($"Tree changed: {writer.Write(etree)}");
            });
        }        
    }

    internal class TimeseriesViewImpl : BindableTimeseries
    {

        private Timeseries _series;

        public TimeseriesViewImpl(Timeseries series)
        {
            _series = series;

            Name = series.Name;
            Id = series.Id;
            Unit = series.CalcUnit;
            Calculated = series.VirtualType == TimeseriesVirtualType.Virtual;

            Values = new List<float> { (float)new Random().NextDouble(), (float)new Random().NextDouble(), (float) new Random().NextDouble(), };
        }
    }

    public class SQLServerDatabaseConnection : IDatabaseConnection
    {
        private string applicationName = "Unknown";

        private Application? app;
        private UserContext? userContext;

        void IDatabaseConnection.AnnouceClientId(string name)
        {
            this.applicationName = name;
        }

        List<BindableProjectDatabase> IDatabaseConnection.LoadProjectDatabaseList()
        {
            List<BindableProjectDatabase> result = new();
            
            app = new Application(new ApplicationContextBuilderOverwriteMessageServer(applicationName, Private.MESSAGE_SERVER));
            if (app.TryLogon(Private.USERNAME, Private.PASSWORD, out userContext))
            {
                SystemDatabase sdb = new SystemDatabase(userContext);
                foreach (DatabaseInfo info in sdb.DatabaseInfos.Where(info => info.IsLeaf).OrderBy(info => info.SortNr))
                {
                    result.Add(new ProjectDatabaseImpl(app, userContext, info));
                }                
            }            

            return result;
        }

        void IDatabaseConnection.CloseConnection()
        {
            if (app != null)
            {
                app.Logoff(userContext);
                app.Dispose();
            }
        }
    }
}
