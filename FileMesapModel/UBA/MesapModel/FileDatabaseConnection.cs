﻿using CsvHelper;

using UBA.MesapModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Formats.Asn1;
using System.Globalization;
using Microsoft.VisualBasic;
using FileMesapModel.Properties;

namespace UBA.MesapModel.File
{
    internal class FileProjectDatabase : BindableProjectDatabase
    {
        internal FileProjectDatabase()
        {
            Name = "CVS File Test Database";
            Description = "Simple file-based database for testing";
            Id = "CSV";
            Guid = "{3D29882E-3C95-4652-AEFE-E4DF92B30A80}";
        }

        private static void IterateCsvFile(string fromFile, string keyField, Action<CsvReader, string> callback)
        {
            using (var reader = new StringReader(fromFile))
            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csvReader.Read();
                csvReader.ReadHeader();

                while (csvReader.Read())
                {
                    csvReader.TryGetField(keyField, out string? keyFieldValue);
                    if (keyFieldValue != null && keyFieldValue.Length > 0)
                    {
                        callback(csvReader, keyFieldValue);
                    }
                }
            }
        }

        public override List<BindableDimension> LoadDimensions()
        {
            List<BindableDimension> dimensions = new List<BindableDimension>();
            IterateCsvFile(Resources.dimensions, "ID", delegate (CsvReader reader, string id)
            {
                dimensions.Add(new FileDimension(reader));
            });
            return dimensions;
        }

        public override List<BindableDescriptor> LoadDescriptors(BindableDimension dimension)
        {
            throw new NotImplementedException();
        }

        public override List<BindableTree> LoadTrees(BindableDimension dimension)
        {
            throw new NotImplementedException();
        }

        public override List<BindableTimeseries> LoadTimeseries(string viewId)
        {
            throw new NotImplementedException();
        }
    }

    internal class FileDimension : BindableDimension
    {
        public FileDimension(CsvReader reader)
        {
            Name = reader.GetField<string>("Name");
            Id = reader.GetField<string>("ID");
        }
    }

    public class FileDatabaseConnection : IDatabaseConnection
    {
        public void AnnouceClientId(string name)
        {
            // No action needed here...
        }

        public List<BindableProjectDatabase> LoadProjectDatabaseList()
        {
            return new List<BindableProjectDatabase> { new FileProjectDatabase() };
        }

        void IDatabaseConnection.CloseConnection()
        {
            // No action needed here...
        }
    }
}
